package com.moez.QKSMS.repository

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.moez.QKSMS.manager.PermissionManagerImpl
import com.moez.QKSMS.util.PhoneNumberUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class ASV_Test {

    @Before
    fun setup() {
        Locale.setDefault(Locale.US)
    }


    // 1 NFR: App moet nummer herkennen of je uit een ander land wordt gebeld of niet
    @Test
    fun compare_IdenticalNsnsWithOneMissingCountryCode_returnsTrue() {
        //arrange
        val context = InstrumentationRegistry.getInstrumentation().context
        val phoneNumberUtils = PhoneNumberUtils(context)
        //act
        val test = phoneNumberUtils.compare("+1 123 456 7890", "123 456 7890")
        //assert
        Assert.assertTrue(test);
    }

    // 2 NFR: Error krijgen als je contact opslaat met fout telefoonnummer
    @Test
    fun isPossibleNumber() {
        //arrange
        val context = InstrumentationRegistry.getInstrumentation().context
        val phoneNumberUtils = PhoneNumberUtils(context)
        //act
        val test = phoneNumberUtils.isPossibleNumber("+1 123 456 7890")
        Assert.assertTrue(test);
    }

    // 3 NFR: Tijdens wachtrij --> "sluit af met hekje". als op iets anders dan 0-9, *, #, + gedrukt wordt moet een error verschijnen
    @Test
    fun isDialable() {
        //arrange
        val context = InstrumentationRegistry.getInstrumentation().context
        val phoneNumberUtils = PhoneNumberUtils(context)
        //act
        val test = phoneNumberUtils.isReallyDialable(':')
        //assert
        Assert.assertFalse(test);
    }

    // 4 FR: Applicatie moet contacten laten zien
    @Test
    fun hasContacts() {
        //arrange
        val context = InstrumentationRegistry.getInstrumentation().context
        val permissionManagerImpl = PermissionManagerImpl(context)
        //act
        val test = permissionManagerImpl.hasContacts()

        //assert
        Assert.assertFalse(test)
    }

    // 5 FR: Apllicatie moet inkomende beloproep laten zien
    @Test
    fun checkIfSomeoneIsCalling() {
        //arrange
        val context = InstrumentationRegistry.getInstrumentation().context
        val permissionManagerImpl = PermissionManagerImpl(context)
        //act
        val test = permissionManagerImpl.hasCalling()
        //assert
        Assert.assertFalse(test)
    }

    // 6 CONSTRAINT: telefoon moet SIMkaart hebben
    @Test
    fun CheckIfSIMCard() {
        //arrange
        val context = InstrumentationRegistry.getInstrumentation().context
        val permissionManagerImpl = PermissionManagerImpl(context)
        //act
        val test = permissionManagerImpl.hasPhone()
        //assert
        Assert.assertFalse(test)
    }

    // 7 NFR: Applicatie moet bestanden kunnen opslaan op de harde schijf van de telefoon
    @Test
    fun CheckIfSpaceFreeOnPhone() {
        //arrange
        val context = InstrumentationRegistry.getInstrumentation().context
        val permissionManagerImpl = PermissionManagerImpl(context)
        //act
        val test = permissionManagerImpl.hasStorage()
        //assert
        Assert.assertFalse(test)
    }

    // 8 FR: Applicatie moet melding weergeven zodra een sms ontvangen is
    @Test
    fun CreateNotificationWhenMessageRecieved() {
        //arrange
        val context = InstrumentationRegistry.getInstrumentation().context
        val permissionManagerImpl = PermissionManagerImpl(context)
        //act
        val test = permissionManagerImpl.hasReadSms()
        //assert
        Assert.assertFalse(test)
    }



}